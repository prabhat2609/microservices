package com.pr.Microservice.currencyexchange.controller;

import java.math.BigDecimal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CurrencyExchangeController {

	@GetMapping(path="/currency-exchange/from/{from}/to/{to}/quantity/{quantity}")
	public String getCurrencyExchange(@PathVariable String from,@PathVariable String to, @PathVariable BigDecimal quantity){
		
		return "currency exchange";
	}
	
}
