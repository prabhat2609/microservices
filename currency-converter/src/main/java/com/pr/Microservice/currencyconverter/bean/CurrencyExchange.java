package com.pr.Microservice.currencyconverter.bean;

import java.math.BigDecimal;

public class CurrencyExchange {

	private int id;
	private String fromCurrency;
	String toCurrency;
	private BigDecimal rate;
	private String env;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFromCurrency() {
		return fromCurrency;
	}
	public void setFromCurrency(String fromCurrency) {
		this.fromCurrency = fromCurrency;
	}
	public String getToCurrency() {
		return toCurrency;
	}
	public void setToCurrency(String toCurrency) {
		this.toCurrency = toCurrency;
	}
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	public String getEnv() {
		return env;
	}
	public void setEnv(String env) {
		this.env = env;
	}
	public CurrencyExchange(int id, String fromCurrency, String toCurrency, BigDecimal rate, String env) {
		super();
		this.id = id;
		this.fromCurrency = fromCurrency;
		this.toCurrency = toCurrency;
		this.rate = rate;
		this.env = env;
	}
	public CurrencyExchange() {
		super();
	}
	@Override
	public String toString() {
		return "CurrencyExchange [id=" + id + ", fromCurrency=" + fromCurrency + ", toCurrency=" + toCurrency
				+ ", rate=" + rate + ", env=" + env + "]";
	}
	
	
	
	
	
	
}
