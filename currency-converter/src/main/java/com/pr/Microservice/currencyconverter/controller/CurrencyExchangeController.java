package com.pr.Microservice.currencyconverter.controller;

import java.math.BigDecimal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.pr.Microservice.currencyconverter.bean.CurrencyExchange;


@RestController
public class CurrencyExchangeController {
	
	@GetMapping(path="/currency-exchange/from/{from}/to/{to}")
	public CurrencyExchange getCurrencyExchange(@PathVariable String from,@PathVariable String to){
	
		CurrencyExchange currencyExchange=new CurrencyExchange(123,from,to,new BigDecimal(78),"8081");
		
		System.out.println("currency exchange--"+currencyExchange);
		return currencyExchange;
	}

}
